console.log("Array Manipulation")

// Basic Array Structure
// Access Elements in an Array - through index

// Two ways to initialize an Array

let array = [1, 2, 3];
console.log(array);

let arr = new Array(1, 2, 3);
console.log(arr);

// index = array.length - 1 
console.log(array[0]);
console.log(array[1]);
console.log(array[2]);

// Array Manipulation

let count = ["one", "two", "three", "four"]
console.log(count.length);
console.log(count[4])

	// using assignment operator (=)
	count[4] = "five"
	console.log(count);

	// Push method array.push()
		// add element at the end of an array
	count.push("element")
	console.log(count)

	function pushMethod(element){
		return count.push(element)
	}
	pushMethod("six")
	pushMethod("seven")
	pushMethod("eight")

	console.log(count)

	// Pop method array.pop()
		// remove the last element of an array

	count.pop();
	console.log(count)

	// Q: Can we use pop method to remove "four" element? -
	count.pop("four")
	console.log(count)

	function popMethod(){
		count.pop()
	}
	popMethod()

	console.log(count)

// Q: How do we add element at the beginning of an array
	// Unshift method array.unshift()
		// adds element at the beginning of an array
	count.unshift("hugot")
	console.log(count)

	function unshiftMethod(name){
		return count.unshift(name)
	}

	unshiftMethod("zero")
	console.log(count)

	// Can we also remove element at the beginning of an array? - 
		// Shift method 	array.shift()
			// removes an element at the beginning of an array
		count.shift()
		console.log(count)

		function shiftMethod(){
			return count.shift();
		}

		shiftMethod()

		console.log(count)

	// Sort method 		array.sort()
	let nums = [15, 32, 61, 130,230, 13, 34];
	nums.sort();

	console.log(nums)

	// sort nums in ascending order
	// a-previous value b-current value

	// accending
	nums.sort(
			function(a, b){
				return a - b
			}
		)
console.log(nums)
// decending
	nums.sort(
			function(a, b){
				return b - a
			}
		)
console.log(nums)

// reverse method 	array.reverse()
nums.reverse()

console.log(nums)

	/* Splice and Slice */

	// Splice method 	array.splice()
			// return	an array of ommited elements
			// 

		// first parameter - index where to start omitting element
		// secong aprameter - # of elements to be ommitted starting from first parameter
		//third parameter - elemets to be added in place of the omitted elements
	console.log(count)
/*
// 1st parameter
	let newSplice = count.splice(1);
		console.log(newSplice);
		console.log(count)
*/

/*
// 2nd parameter
	let newSplice = count.splice(1, 2);
	console.log(newSplice)
	console.log(count)
*/
/*
// 3rd parameter
	let newSplice = count.splice(1, 2, "a1", "b2", "c3")
	console.log(newSplice)
	console.log(count)
*/

// Slice method 	array.slice(start, end)

	// first aprameter - index where to begin omitting elements
	// second parameter - # of elements to be omitted ( index - array.lenght )
console.log(count)
/*
1st parameter
let newSlice = count.slice(1);
console.log(newSlice);*/

// 2nd parameter
let newSlice = count.slice(3, 6);
console.log(newSlice);

// Concat method 	array.concat()
	// use to merge two or more arryas
console.log(count)
console.log(nums)
let animals = ["birds", "cat", "dog", "fish"]

let newConcat = count.concat(nums, animals)
console.log(newConcat);

// Join method 		array.join() //"", 
	// 
	let meal = ["rice", "steak", "juice"];

	let newJoin = meal.join()
	console.log(newJoin)

	newJoin = meal.join("")
	console.log(newJoin)
		newJoin = meal.join(" ")
	console.log(newJoin)
	newJoin = meal.join("-")
	console.log(newJoin)



// toString method
console.log(nums)
console.log(typeof nums[3])

let newString = nums.toString()
console.log(typeof newString)

/*Accessors*/
let countries = ["US", "PH", "CAN", "PH", "SG", "HK", "PH", "NZ"]
	// indexOf()

		// finds the index of a given element where it is "first" found.
		let index = countries.indexOf("PH")
		console.log(index)

		// if elements is non existing, return is -1
		let example = countries.indexOf("AU")
		console.log(example)

	// lastIndexOf
	let lastIndex = countries.lastIndexOf("PH")
	console.log(lastIndex)

/*Iterators*/
// cb - call back functions
	// forEach(cb())	array.forEach
	// map()		array.map()

let days = ["mon", "tue", "wed", "thu", "fri", "sat", "sun" ];

// forEach
	// returns undefined
let daysOfTheWeek = days.forEach(
		function(element){
			console.log(element)
		}
	)
console.log(daysOfTheWeek)

// map
	// reaasign a value and create an new array
let mapDays = days.map(function(day){
	return `${days} is the day of the week`
})
console.log(mapDays)
console.log(days)

let days2 = [];
console.log(days)

days.forEach(function(days){
	days2.push(days);
})
console.log(days2)

// filter 	array.filter(cb())
console.log(nums)

let newFilter = nums.filter(function(num){
	return num < 50
})
console.log(newFilter)

// includes array.includes
	// returns boolean value if a word is existing in an array
console.log(animals)
// includes 	array.includes()
let newIncludes = animals.includes("cat")
console.log(newIncludes)

/*mini-activity*/

/*function miniActivity(value){
	if (animal.includes()){
		return `${value} is found`;
	} else {
		return `${value} is not found`
	}
}
console.log(miniActivity("dog"))
console.log(miniActivity("man"))
console.log(miniActivity("fish"))*/


// every (cb())
	// returns boolean value
	// returns true only if "all elements" passed the given coondition


let newEvery =  nums.every(function(num){
	return ( num > 100)
})
console.log(newEvery)


// some (cb())
	//boolean

	let newSome =  nums.some(function(num){
		return ( num > 50)
	})
	console.log(newSome)


// reduce (cb(<previous>, <current>))
let newReduce = nums.reduce(function(a, b){
	return a + b
})
console.log(newReduce)

// to get the average of nums array
	// total all elements
	// divide it by total # of elements
	let averange = newReduce/ nums.length

//____________________________________________________

	// toFixed( # of decimals - returns string)

console.log(averange.toFixed(2))

	// parseInt() or parseFloat()
	console.log(parseInt(averange))

	console.log(parseInt(averange.toFixed(2)))
	console.log(parseFloat(averange.toFixed(2)))
